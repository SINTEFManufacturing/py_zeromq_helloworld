# py_zeromq_helloworld

## Description
A simple package showing how to use pyzmq.


## Installation
Not ment for installation, but package is set up using Poetry: https://python-poetry.org/

## Usage
There are two example scripts provided in the folder "py_zeromq_helloworld":

## Support
You can ask Eirik: eirik.njaastad@sintef.no

## Author
Made by Eirik B. Njåstad, SINTEF Manufacturing in 2024.

## License
GPLv3
