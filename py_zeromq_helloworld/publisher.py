__author__ = "Eirik Njaastad"
__copyright__ = "SINTEF Manufacturing 2024"
__credits__ = ["Eirik Njaastad"]
__license__ = "GPLv3"
__maintainer__ = "Eirik Njaastad"
__email__ = "eirik.njaastad@sintef.no"
__status__ = "Development"

import zmq
import random
import time

# Define the port number
port = "5556"

# Initialize ZeroMQ context and socket
context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind(f"tcp://*:{port}")

# Main loop to publish messages
while True:
    # Generate random topic and message data
    topic = random.randrange(999, 1010)
    messagedata = random.randrange(1, 50)

    # Print the topic and message data for debugging
    print(f"{topic} {messagedata}")

    # Send the message as a string over the socket
    socket.send_string(f"{topic} {messagedata}")

    # Wait for half a second before publishing the next message
    time.sleep(0.5)
