__author__ = "Eirik Njaastad"
__copyright__ = "SINTEF Manufacturing 2024"
__credits__ = ["Eirik Njaastad"]
__license__ = "GPLv3"
__maintainer__ = "Eirik Njaastad"
__email__ = "eirik.njaastad@sintef.no"
__status__ = "Development"

import zmq

port = "5556"

# Socket to subscribe to the server
context = zmq.Context()
socket = context.socket(zmq.SUB)

# Connect to the server
print("Collecting updates from server...")
socket.connect(f"tcp://localhost:{port}")

# Specify the topic to subscribe to
topic_filter = "1005"
socket.setsockopt_string(zmq.SUBSCRIBE, topic_filter)

# Process 15 updates
total_value = 0
for update_nbr in range(15):
    # Receive the message from the socket
    string = socket.recv()

    # Split the received message into topic and message data
    topic, messagedata = string.split()

    # Convert message data to integer and accumulate the total value
    total_value += int(messagedata)

    # Print topic and message data
    print(topic, messagedata)

# Calculate and print the average message data value for the subscribed topic
average_value = total_value / 15 if update_nbr != 0 else 0  # Avoid division by zero
print("Average message data value for topic " f"'{topic_filter}' was {average_value}")
